# Sociallinder

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.12.1.

## Build & development

Run `grunt` for building and `grunt serve` for preview.
Run `grunt serve dist` to run the final version

## Testing

Running `grunt test` will run the unit tests with karma.


## Redirecting to HTTPS on appspot.com
https://cloud.google.com/appengine/docs/ssl

## Redirecting to HTTPS on custom domain
https://cloud.google.com/appengine/docs/java/config/webxml?csw=1#Secure_URLs

## frameworks
http://inorganik.github.io/countUp.js/