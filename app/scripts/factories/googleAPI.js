/*jshint unused: false*/

'use strict';
function googleOnLoadCallback(){  
    var apisToLoad = 1; // must match number of calls to gapi.client.load()  
    var gCallback = function() {  
        if (--apisToLoad === 0) {  
            //Manual bootstraping of the application  
            var $injector = angular.bootstrap(document, ['sociallinderApp']);  
            console.log('Angular bootstrap complete ');  
            console.log(gapi);
        }
    };  
    var ROOT;
    if (window.location.host === 'localhost:9000' || window.location.host === 'localhost:8080'){
    	ROOT = '//' + window.location.host + '/_ah/api';
    }
    else{
    	ROOT = 'https://' + window.location.host + '/_ah/api';
    }

    gapi.client.load('sociallinder', 'v1', gCallback, ROOT); 
    console.log(ROOT);
}

angular.module("sociallinderApp")
	.factory('api', function($q, $http, $location){

        return {
            createUser: function(userObject){
                var defer = $q.defer();
                gapi.client.sociallinder.userApi.createUser(userObject)
                .then(function(resp){
                    defer.resolve(resp.result); //?
                }, function(reason){
                    console.log('Error: ' + reason.result.error.message);
                    defer.reject(reason.result.error.message);
                });
                return defer.promise;
            },

			testBackendConn: function(){
				var defer = $q.defer();
				gapi.client.sociallinder.mainApi.helloWorld()
				.then(function(resp){
					defer.resolve(resp.result.message);
				}, function(reason){
            		console.log('Error: ' + reason.result.error.message);
            		defer.reject(reason.result.error.message);
				});
				return defer.promise;
			},

            getAllProjects: function(){
                var defer = $q.defer();
                gapi.client.sociallinder.projectApi.getAllProjects()
                .then(function(resp){
                    defer.resolve(resp.result.items);
                }, function(reason){
                    console.log('Error: ' + reason.result.error.message);
                    defer.reject(reason.result.error.message);
                });
                return defer.promise;
            },

            getProjectsByType: function(projectType){
                var defer = $q.defer();
                console.log(projectType);
                gapi.client.sociallinder.projectApi.getProjectByProjectType(projectType) //PARAMETER correct?? 
                .then(function(resp){
                    defer.resolve(resp.result.items);
                }, function(reason){
                    console.log('Error: ' + reason.result.error.message);
                    defer.reject(reason.result.error.message);
                });
                return defer.promise;
            },

            getProjectById: function(projectID){
                var defer = $q.defer();
                gapi.client.sociallinder.projectApi.getProjectById(projectID)
                .then(function(resp){
                    defer.resolve(resp.result);
                }, function(reason){
                    console.log('Error: ' + reason.result.error.message);
                    defer.reject(reason.result.error.message);
                });
                return defer.promise;
            },

            getProjectByUserId: function(userID){
                var defer = $q.defer();
                gapi.client.sociallinder.projectApi.getProjectbyUserId(userID)
                .then(function(resp){
                    defer.resolve(resp.result);
                }, function(reason){
                    console.log('Error: ' + reason.result.error.message);
                    defer.reject(reason.result.error.message);
                });
                return defer.promise;
            },

            saveProject: function(projectObject){
                var defer = $q.defer();
                gapi.client.sociallinder.projectApi.saveProject(projectObject)
                .then(function(resp){
                    defer.resolve(resp.result); //?
                }, function(reason){
                    console.log('Error: ' + reason.result.error.message);
                    defer.reject(reason.result.error.message);
                });
                return defer.promise;
            },

            findUserByEmail: function(email, password){
                var defer = $q.defer();
                gapi.client.sociallinder.userApi.findUserByEmail(email, password)
                .then(function(resp){
                    defer.resolve(resp.result);
                }, function(reason){
                    console.log('Error: ' + reason.result.error.message);
                    defer.reject(reason.result.error.message);
                });
                return defer.promise;
            },

            addKeyNeedsbyId: function(knobject){
                var defer = $q.defer();
                gapi.client.sociallinder.projectApi.addKeyNeedsbyId(knobject)
                .then(function(resp){
                    defer.resolve(resp.result);
                }, function(reason){
                    console.log('Error: ' + reason.result.error.message);
                    defer.reject(reason.result.error.message);
                });
                return defer.promise;
            },

            saveUserDetails: function(userobject){
                var defer = $q.defer();
                gapi.client.sociallinder.userApi.saveUserDetails(userobject)
                .then(function(resp){
                    defer.resolve(resp.result);
                }, function(reason){
                    console.log('Error: ' + reason.result.error.message);
                    defer.reject(reason.result.error.message);
                });
                return defer.promise;
            },

            supportProject: function(kid, pid, dnobject){
                var defer = $q.defer();
                gapi.client.sociallinder.projectApi.supportProject(kid, pid, dnobject)
                .then(function(resp){
                    defer.resolve(resp.result);
                }, function(reason){
                    console.log('Error: ' + reason.result.error.message);
                    defer.reject(reason.result.error.message);
                });
                return defer.promise;
            },



            getUserById: function(userID){
                var defer = $q.defer();
                gapi.client.sociallinder.userApi.getUserById(userID)
                .then(function(resp){
                    defer.resolve(resp.result);
                }, function(reason){
                    console.log('Error: ' + reason.result.error.message);
                    defer.reject(reason.result.error.message);
                });
                return defer.promise;
            },

            getImageUploadUrl: function(projectId){
                var defer = $q.defer();
                gapi.client.sociallinder.projectApi.getImageUploadCallbackUrl({
                    'projectId' : projectId
                })
                .then(function(resp){
                    console.log("api.getImageUploadUrl returned with:");
                    console.log(resp);
                    var message = resp.result.message;
                    if(message.indexOf("8080") > -1){
                        console.log("splitting url at 8080");
                        var split = message.split('8080'); //pretty crappy but works locally --> lets see how it works on the cloud server   
                        defer.resolve(split[1]);
                    }
                    else{
                        console.log("returning raw message");
                        defer.resolve(message);
                    }
                }, function(reason){
                    console.log('Error: ' + reason.result.error.message);
                    defer.reject(reason.result.error.message);
                });
                console.log("the returned defer is:");
                console.log(defer);
                return defer.promise;
            },

            getNumberOfSupportersByProjectID: function(projectID){
                var defer = $q.defer();
                gapi.client.sociallinder.projectApi.getNumberOfSupportersByProjectID(projectID)
                .then(function(resp){
                    defer.resolve(resp.result);
                }, function(reason){
                    console.log('Error: ' + reason.result.error.message);
                    defer.reject(reason.result.error.message);
                });
                return defer.promise;
            },

            
            getPercentageNeedMetByProjectID: function(projectID){
                var defer = $q.defer();
                gapi.client.sociallinder.projectApi.getPercentageNeedMetByProjectID(projectID)
                .then(function(resp){
                    defer.resolve(resp.result);
                }, function(reason){
                    console.log('Error: ' + reason.result.error.message);
                    defer.reject(reason.result.error.message);
                });
                return defer.promise;
            },

            getProjectImageUrl: function(blobKey){
                var host = $location.host();
                var defer = $q.defer();
                if(host.indexOf("localhost") > -1){
                    console.log("calling localhost to serve image...");
                    gapi.client.sociallinder.projectApi.serveImage({
                        'blobkey' : blobKey
                    })
                    .then(function(resp){
                        var message = resp.result.message;
                        console.log("splitting url at 8080");
                        var split = message.split('8080'); //pretty crappy but works locally --> lets see how it works on the cloud server   
                        defer.resolve(split[1]);
                        
                    }, function(reason){
                        console.log('Error: '+reason.result.error.message);
                        defer.reject(reason.result.error.message);
                    });
                }
                else{
                    console.log("calling pink-elephnt to serve image...");
                    $http({
                        url: "https://pink-elephnt.appspot.com/_ah/api/sociallinder/v1/serveImage", 
                        method: "GET",
                        params: {blobkey: blobKey}
                     })
                    .then(function(resp){
                        console.log(resp);
                        var message = resp.data.message;
                        console.log(message);
                        defer.resolve(message);
                    }, function(resp){
                        console.log(resp);
                        defer.reject(resp);
                    });
                }
                return defer.promise;
            }
		};
	});

/**
 * sample coding: you should read a few things about "promise"s 
 * when it comes to frontend async data loading. this is pretty
 * much the standard way of doing it. in the API methods I create those 
 * promises myself ($q.defer() returns an empty promise). when the function is
 * successful "resolve" gets executed (you can pass in data). otherwise "reject". 
 * I create the promises myself to create an abstraction layer for error handling
 * and making the datahandling in the calling methods a little easier.
 *
 * API Method:
 * 
 * getClassInstances: function(start, end){
 *  var defer = $q.defer();
 *   gapi.client.yoganey.classAPI.getClassInstances({
 *      'account_id' : accountId,
 *       'start_date': start.format("YYYY-MM-DD[T]HH:mm:ss"),
 *       'end_date': end.format("YYYY-MM-DD[T]HH:mm:ss")
 *   }).then(function(resp){
 *       defer.resolve(resp.result.items);
 *   }, function(reason){
 *       console.log('Error: ' + reason.result.error.message);
 *       defer.reject(reason.result.error.message);
 *   });
 *   return defer.promise;
 *
 * Calling Method:
 *
 * api.getClassInstances(start,end).then(function(classInstances){
 *   var events = [];
 *   //read the instances, transform them into the fullcalendar-event-representation and let callback know.
 *   for(var i=0; i < classInstances.length; i++){
 *       var instance = classInstances[i];
 *       events.push({
 *           id: instance.classId,
 *           title: instance.title,
 *           start: moment.utc(instance.startTime).format('YYYY-MM-DD HH:mm:ss'),
 *           end: moment.utc(instance.endTime).format('YYYY-MM-DD HH:mm:ss'),
 *           description: instance.description
 *       });
 *   }
 *   $scope.data.events = events;
 * 
 */