
/*jshint unused: false*/
/*jshint -W117 */
/*jslint bitwise: true */

'use strict';

angular.module('sociallinderApp')
    .factory('AuthenticationService', function($http, $cookies, $rootScope, $timeout, api, $q){
    //Helper stuff to encode and decode
    var Base64 = {

            keyStr: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=',
     
            encode: function (input) {
                var output = "";
                var chr1, chr2, chr3 = "";
                var enc1, enc2, enc3, enc4 = "";
                var i = 0;
     
                do {
                    chr1 = input.charCodeAt(i++);
                    chr2 = input.charCodeAt(i++);
                    chr3 = input.charCodeAt(i++);
     
                    enc1 = chr1 >> 2;
                    enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                    enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                    enc4 = chr3 & 63;
     
                    if (isNaN(chr2)) {
                        enc3 = enc4 = 64;
                    } else if (isNaN(chr3)) {
                        enc4 = 64;
                    }
     
                    output = output +
                        this.keyStr.charAt(enc1) +
                        this.keyStr.charAt(enc2) +
                        this.keyStr.charAt(enc3) +
                        this.keyStr.charAt(enc4);
                    chr1 = chr2 = chr3 = "";
                    enc1 = enc2 = enc3 = enc4 = "";
                } while (i < input.length);
     
                return output;
            },
     
            decode: function (input) {
                var output = "";
                var chr1, chr2, chr3 = "";
                var enc1, enc2, enc3, enc4 = "";
                var i = 0;
     
                // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
                var base64test = /[^A-Za-z0-9\+\/\=]/g;
                if (base64test.exec(input)) {
                    window.alert("There were invalid base64 characters in the input text.\n" +
                        "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                        "Expect errors in decoding.");
                }
                input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
     
                do {
                    enc1 = this.keyStr.indexOf(input.charAt(i++));
                    enc2 = this.keyStr.indexOf(input.charAt(i++));
                    enc3 = this.keyStr.indexOf(input.charAt(i++));
                    enc4 = this.keyStr.indexOf(input.charAt(i++));
     
                    chr1 = (enc1 << 2) | (enc2 >> 4);
                    chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                    chr3 = ((enc3 & 3) << 6) | enc4;
     
                    output = output + String.fromCharCode(chr1);
     
                    if (enc3 !== 64) {
                        output = output + String.fromCharCode(chr2);
                    }
                    if (enc4 !== 64) {
                        output = output + String.fromCharCode(chr3);
                    }
     
                    chr1 = chr2 = chr3 = "";
                    enc1 = enc2 = enc3 = enc4 = "";
     
                } while (i < input.length);
     
                return output;
            }
        };


        var service = {};

        // service.Login = Login;
        // service.SetCredentials = SetCredentials;
        // service.ClearCredentials = ClearCredentials;
        // service.loginStatus = loginStatus;
        
        service.Login = function(username, password, callback) {
            api.findUserByEmail({"email" : username, "password": password}).then(function(result){
                    var response;
                    console.log(result);
                    if (result){
                        alert("Hey, "+result.userName+" und willkommen zurück :)");
                        response = { 
                            success: true,
                            user: result
                        };
                    }
                    else{
                        alert("Ups, bist du schon registriert?");
                        response = { success: false, message: 'Username or password is incorrect' };
                    }
                    callback(response);
                });

        };

        service.LoginWithId = function(userId, callback){
            api.getUserById({"id" : userId})
                .then(function(result){
                    var response;
                    if(result){
                        response = { 
                            success: true,
                            user: result
                        };
                    }
                    else{
                        response = {
                            success : false,
                            message : "User could not be loggedIn"
                        };
                    }
                    callback(response);
                });
        };

        service.getLoginLabel = function() {
            var credentials = service.getCredentials();
            if (credentials !== undefined){
                if(credentials.user.userName !== undefined){
                    return credentials.user.userName;
                }
                else if(credentials.user.email){
                    return credentials.user.email;
                }
                else{
                    return credentials.user.userId;
                }
            }
            else{
                return("Login");
            }
        };

        service.getCredentials = function(){
            var defer = $q.defer();
            var userId = $cookies.get('pinkelephantLogin');
            var resolved = false;
            var credentials;

            if($rootScope.globals !== undefined){
                if($rootScope.globals.currentUser !== undefined){
                     credentials = $rootScope.globals.currentUser;
                     resolved = true;
                }
            }
            if (!resolved){
                if (userId === undefined || userId === null){
                    resolved = true;
                }
                else{
                    service.LoginWithId(userId, function(resp){
                        if(resp.success){
                            service.SetCredentials(resp.user);
                            resolved = true;
                        }   
                        else{
                            resolved = true;
                        }
                    });
                }
            }
            return credentials;
        };

        service.SetCredentials = function(user) {
            var authdata = Base64.encode(user.userName + ':' + user.password);

            if($rootScope.globals === undefined){
                $rootScope.globals = {};
            }
            $rootScope.globals.currentUser = {user: user};
                
            $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata; // jshint ignore:line
            $cookies.put('pinkelephantLogin', $rootScope.globals.currentUser.user.userId);
        };

        // service.ClearCredentials = function() {
        //     $rootScope.globals = {};
        //     $cookies.remove('pinkelephantLogin');
        //     $http.defaults.headers.common.Authorization = 'Basic ';
        // };
        
        

 
        service.clearCredentials = function() {
            if($rootScope.globals!==undefined){
                $rootScope.globals.currentUser = undefined;
                $cookies.remove('pinkelephantLogin');
                $http.defaults.headers.common.Authorization = 'Basic ';
            }        
        };

        return service;
});
 
    
