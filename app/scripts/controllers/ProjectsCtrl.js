/*jshint unused: false*/
/*jshint -W117 */
'use strict';

/**
 * @ngdoc function
 * @name sociallinderApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the sociallinderApp
 */
angular.module('sociallinderApp')
  .controller('ProjectsCtrl', function ($scope, $rootScope, api, $location, $state, AuthenticationService) {
  	//On initial load of Controller
    var user = AuthenticationService.getCredentials();


    $scope.projectTypesSpecified = function(user){
      for (var i=0; i < user.projectType.length; i++){
        if(user.projectType[i] !== null){
          return true;
        }
      }
      return false;
    };


    $scope.getAllProjects = function() {

    api.getAllProjects().then(function(projectInstances){
  		var projects = [];

      var funcGetNeedsMet = function(projectId, index){
        api.getPercentageNeedMetByProjectID({"ProjectID": projectId})
        .then(function(response){
            projects[index].needsMet = response.message/100;
        });
      };

  		for(var i=0; i < projectInstances.length; i++){
  			var instance = projectInstances[i];
  			projects.push({
  				id: instance.id,
          title: instance.projectTitle,
          description: instance.projectDescription,
          sponsor: instance.organizationId,
          imageUrl: instance.imageUrl,
          ideaFrom: instance.user.userName,
          needsMet: 0
  			});	
  			//console.log('Pushing one element to scope with title ' + instance.projectTitle + '.');
        funcGetNeedsMet(instance.id, i);
   		}
   		$scope.allProjects = projects;
   	});
    };

    $scope.getAllProjects();

    if (AuthenticationService.getCredentials()!==undefined){
        api.getProjectByUserId({"UserId": $rootScope.globals.currentUser.user.userId}).then(function(projectInstances){
          var userProjects = [];
          for(var i=0; i < projectInstances.items.length; i++){
            var instance = projectInstances.items[i];
            userProjects.push({
              id: instance.id,
              title: instance.projectTitle,
              description: instance.projectDescription,
              sponsor: instance.organizationId 
            }); 
            //console.log('Pushing one element to scope with title ' + instance.projectTitle + '.');
          }
          $scope.myProjects = userProjects;
         // console.log($scope.myProjects);
        },
            function(reason){ console.log("failed");}
        );
    }
    var funcGetNeedsMetSuggestedProjects = function(projectId, index){
        api.getPercentageNeedMetByProjectID({"ProjectID": projectId})
        .then(function(response){
            console.log(response.message/100);
            $scope.suggestedProjects[index].needsMet = response.message/100;
        });
      };

    //Get project suggestions for a user
    if (user!==undefined){
      if(user.user.organization === false) {
      //get Projects that satisfy user Interests criterion (only the first four..)
      if ($scope.projectTypesSpecified($rootScope.globals.currentUser.user)) {
          api.getProjectsByType({"Project Type" : [$rootScope.globals.currentUser.user.projectType[0], $rootScope.globals.currentUser.user.projectType[1], $rootScope.globals.currentUser.user.projectType[2], $rootScope.globals.currentUser.user.projectType[3], $rootScope.globals.currentUser.user.projectType[4]]}).then(function(projectInstances){
             console.log("suggestedProjectsFound: ");
             console.log(projectInstances);
             $scope.suggestedProjects = [];
             for(var i=0; i < projectInstances.length; i++){
               var instance = projectInstances[i];
                $scope.suggestedProjects.push({
                id: instance.id,
                title: instance.projectTitle,
                description: instance.projectDescription,
                sponsor: instance.organizationId,
                imageUrl: instance.imageUrl,
                ideaFrom: instance.user.userName,
                needsMet: 0
              });
              funcGetNeedsMetSuggestedProjects(instance.id, i); 
              console.log('Pushing one element to scope with title ' + instance.projectTitle + '.');
             }
          if ($scope.allProjects === []) { 
            console.log("no projects found");
          } else{
            // $scope.suggestedProjects = projects; 
          }
          },
          function(reason) { 
            console.log("failed");
            //Flag for no projects?
          }
        );
       } else { console.log("no project types specified");} 
      } else { console.log("user is organization");}

    } else { console.log("no User logged in");}
    

    //Declaring objects for filters
    $scope.select = {};
 
   	//Controller functions
    $scope.go = function (state, params) {
      console.log(state+"/"+params);
      $state.go(state,params);
      // $location.path(path);
    };

    $scope.gotoProjectCreation = function(state, params){
      //show modal to make user login. (use login-directive) --> can be done later      
      $scope.go(state,params);
    };

    $scope.isUserLoggedIn = function(){
        var credentials = AuthenticationService.getCredentials();
        if(credentials !== undefined){
            return true;
            //$rootScope.globals.currentUser.user = User Object
        }
        else{
            return false;
        }
    };

    $scope.isUserOrganization = function(){
      var credentials = AuthenticationService.getCredentials();
      if (credentials !== undefined) {
        if (credentials.user.organization) {
            return true;
        } else{
            return false;
        }
      } else {
        return false;
      }
    };

      var funcGetNeedsMet = function(projectId, index){
        api.getPercentageNeedMetByProjectID({"ProjectID": projectId})
        .then(function(response){
            console.log(response.message/100);
            $scope.allProjects[index].needsMet = response.message/100;
        });
      };
    //Only by Filter ProjectType for now -- add Support filter!
    $scope.getProjectsByFilter = function(){
      //get Projects that satisfy filter criterion
      if ($scope.select.cause) { //check if type is not empty
          api.getProjectsByType({"Project Type" : $scope.select.cause}).then(function(projectInstances){
             $scope.allProjects = [];
             for(var i=0; i < projectInstances.length; i++){
               var instance = projectInstances[i];
                $scope.allProjects.push({
                id: instance.id,
                title: instance.projectTitle,
                description: instance.projectDescription,
                sponsor: instance.organizationId,
                imageUrl: instance.imageUrl,
                ideaFrom: instance.user.userName,
                needsMet: 0
              });
              funcGetNeedsMet(instance.id, i); 
                //console.log('Pushing one element to scope with title ' + instance.projectTitle + '.');
             }
         // $scope.allProjects = projects;
         console.log("allProjects");
         console.log($scope.allProjects);
        });
      }
      else{
        //Alternative: Get all?
          $scope.getAllProjects();
       // alert('Bitte geben sie ein Soziales Anliegen an!'); //'Please specify a social cause!'
      }
    };
    
  });
