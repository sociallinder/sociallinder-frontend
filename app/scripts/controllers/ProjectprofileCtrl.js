/*jshint unused: false*/
/*jshint -W117 */
'use strict';

/**
 * @ngdoc function
 * @name sociallinderApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the sociallinderApp
 */

angular.module('sociallinderApp')
  .controller('ProjectprofileCtrl', function ($scope, api, $stateParams, $location, AuthenticationService, $state, $uibModal) {

   //console.log($stateParams.projectId);
    this.initial={
        value: 0
    };

    $scope.page=1;

    //On initial load of Controller
    api.getProjectById({"id" : $stateParams.projectId}).then(function(projectInstance){
        console.log(projectInstance);
        var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
        var startDate = new Date(projectInstance.startDate);
        var endDate = new Date(projectInstance.endDate);
        console.log(moment.duration(moment().diff(moment(projectInstance.startDate))).asDays());
        var project = {
                id: projectInstance.id,
                title: projectInstance.projectTitle,
                description: projectInstance.projectDescription,
                totalAmount: projectInstance.totalAmount,
                totalFunded: projectInstance.totalFunded,
                //totalNeedsMet: projectInstance.totalFunded / projectInstance.totalAmount;
                totalCountSupporter: 0,
                totalNeedsMet: 0,  //will be calculated later
                // daysSinceProjectStart: Math.round(Math.abs((startDate.getTime() - endDate.getTime())/(oneDay))),
                daysSinceProjectStart: Math.round(moment.duration(moment().diff(moment(projectInstance.startDate))).asDays()),
                sponsor: projectInstance.user.organizationObject.organizationTitle,
                ideaFrom: projectInstance.user.userName,
                successFactors: projectInstance.successFactors,
                keyNeeds: projectInstance.keyNeeds,
                imageUrl: projectInstance.imageUrl

                // 
// {
 //   "title": "First Key Need",
 //   "totalNeeds": 10
 //   "needsMet": 0
 //  },
 //  {
 //   "title": "Second Keyned",
 //   "totalNeeds": 1000
 //  }
 // ]
                // [  
                //     {
                //         title: 'Spenden',
                //         details:[
                //             {
                //                 text: '123 Menschen haben bereits gespendet.'
                //             }
                //         ]
                //     },
                //     {
                //         title: 'Technisches Equipment',
                //         details:[
                //             {
                //                 text: '1 Stereoanlage'
                //             },
                //             {
                //                 text: '2 Fußballtore'
                //             }
                //         ]
                //     },
                //     {
                //         title: 'Verpflegung',
                //         details:[
                //             {
                //                 text: 'Wasserkästen'
                //             },
                //             {
                //                 text: 'Grillfleisch'
                //             }
                //         ]
                //     }
                // ] 
        }; 
        console.log('Pushing one element to scope with title ' + projectInstance.projectTitle + '.');

        //console.log(projectInstance.keyNeeds);

        //console.log(projectInstance);

        // api.getProjectImageUrl(project.imageBlobKey)
        //     .then(function(imageurl){
        //         project.imageUrl = imageurl;
        //     });

        api.getNumberOfSupportersByProjectID({"ProjectID": project.id})
        .then(function(response){
            project.totalCountSupporter = response.message;
        });

        api.getPercentageNeedMetByProjectID({"ProjectID": project.id})
        .then(function(response){
            project.totalNeedsMet = response.message/100;
            console.log(response.message);
        });

        $scope.profileProject = project;
    });

    


    //Detail data
    this.details = [{
        lid:0,
        lstyle: 'float:left;',
        link: '.campaign',
        llabel : 'Kampagne',
        lclass : 'nav-a'
    },
    {
        lid:1,
        link: '.events',
        lstyle: 'float:left; margin-left:40px;',
        llabel : 'Termine',
        lclass : 'nav-a'
    },
    {
        lid:2,
        link: '.contact',
        lstyle: 'float:left; margin-left:40px;',
        llabel : 'Kontakt',
        lclass : 'nav-a'
    },
    {
        lid:3,
        link: '.feedback',
        lstyle: 'float:left; margin-left:40px;',
        llabel : 'Feedback',
        lclass : 'nav-a'
    },
    ];

    this.getImageUrl = function(blobkey){
        // api.getProjectImageUrl(blobkey)
        //     .then(function(imageurl){
        //         return imageurl;
        //     });
    };


    this.getCategory = function(item) {
    this.details.selected = item.lid;
    };

    this.details.selected = 0;

    this.isActive = function(item) { 
    return this.details.selected === item.lid;
    };


    $scope.open = function (size) {

    $scope.modalInstance = $uibModal.open({
      animation: false,
      templateUrl: 'myModalContent2.html',
      controller: 'ProjectprofileCtrl',
      size: size,
      scope: $scope
     
    });

    
    $scope.modalInstance.result.then(function () {
      console.log('modal closed');
    }, function () {
       console.log('modal canceled');
    });
    };

    $scope.ok = function () {

        if($scope.isUserLoggedIn()){
            if ($scope.page<2){
                $scope.page=$scope.page+1;
                $scope.Label = "Bestätigen";
            }
            else{
                    
                    if ($scope.selectedItem.totalNeeds>=$scope.selectedItem.needsMet+$scope.donation){
                        $scope.totalneedMet=$scope.selectedItem.needsMet+$scope.donation;
                        var knobject = {
                            "needsMet":$scope.totalneedMet,
                            "description":$scope.selectedItem.description,
                            "dimension":$scope.selectedItem.dimension,
                            "id":$scope.selectedItem.id,
                            "title":$scope.selectedItem.title,
                            "totalNeeds":$scope.selectedItem.totalNeeds,
                        };

                            console.log(knobject);
                            api.addKeyNeedsbyId(knobject).then(function(projectInstance){
                                console.log("KeyNeedDTO Response:");
                                console.log(projectInstance);
                            });

                       var dnobject = {     
                            "amount": $scope.donation,
                            "keyNeedID": $scope.selectedItem.id,
                            "keyNeedTitle": $scope.selectedItem.title,
                            "userId": AuthenticationService.getCredentials().user.userId,
                        };

                        api.supportProject({"keyneedid":$scope.selectedItem.id, "projectid":$stateParams.projectId}, dnobject).then(function(donationInstance){
                                console.log("DonationDTO Response:");
                                console.log(donationInstance);
                                $state.go($state.current, {}, {reload: true});
                                //$location.path("'"+$location.path()+"'");
                            });

                        $scope.modalInstance.close();

                    }
                    else{
                        alert("Spendenbetrag größer als fehlender Bedarf");
                    }

            }
        }
        else{
            $scope.modalInstance.close();
            $state.go("login");
        }
    
    };

    $scope.cancel = function () {
    $scope.modalInstance.dismiss();
    };

    $scope.isUserLoggedIn = function(){
      var credentials = AuthenticationService.getCredentials();
      if(credentials !== undefined){
        return true;
      }
      else{
        return false;
      }
    };

    if($scope.isUserLoggedIn()){
        $scope.Label ="Weiter"; 
    }
    else{
        $scope.Label = "Login";
    } 

    $scope.supportsubmit = function() { 


        
    };
  });