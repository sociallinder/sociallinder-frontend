/*jshint unused: false*/
'use strict';

/**
 * @ngdoc function
 * @name sociallinderApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the sociallinderApp
 */
angular.module('sociallinderApp')
  .controller('IdeasCtrl', function ($scope, fileUpload, api) {
  	$scope.data = {};
  	$scope.data.customImage = '/images/raising-hands.jpg';


  	$scope.uploadImage = function(){
  		api.getImageUploadUrl(123) //123 shall become the projectId that is returned from the "createProject" service
  			.then(function(uploadUrl){
				var file = $scope.myFile;
		        fileUpload.uploadFileToUrl(file, uploadUrl)
		        	.then(function(blobkey){
                api.getProjectImageUrl(blobkey)
                  .then(function(imageurl){                  
                    $scope.data.customImage = imageurl+'=s300';
                  });
		        	});
  			});


  	};
  });
