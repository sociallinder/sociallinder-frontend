/*jshint unused: false*/
/*jshint -W117 */
'use strict';

/**
 * @ngdoc function
 * @name sociallinderApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the sociallinderApp
 */
angular.module('sociallinderApp')
	.controller('RegisterCtrl', function($scope, api, $state){


		$scope.firstname="";
		$scope.lastname="";
		$scope.userObject = {
			email : "",
			address : "",
			password : "",
			mainGoal : "",
			personalwebsite : "",
			userName : "",
			hobbys : "",
			birthDate : "",
			organization: "false",
			organizationObject:{
				organizationTitle: "",
				address: "",
				phoneNumber: ""
			}

		};

		$scope.obj = {test: "message"};

		$scope.submit = function(){
			var name = $scope.firstname + " " + $scope.lastname;
			if(name !== " "){
				$scope.userObject.userName = name;
			}
			console.log($scope.userObject);
			api.createUser($scope.userObject).then(
	        function(response){
	          alert('Benutzer erfolgreich registriert!'); //User succesfully registered!
	          console.log(response);
	          $scope.go("login",{});
	        },
	        function(reason){} //What to do when it is not successful?
	      );
		};

		$scope.setIsOrganization = function(bool){
			$scope.userObject.organization = bool;
		};

		$scope.buttonIsActive = function(bool){
			if(bool === $scope.userObject.organization){
				return 'active';
			}
		};

		$scope.go = function (state, params) {
      		console.log(state+"/"+params);
	      	$state.go(state,params);
	    };


		$(".date-picker").datepicker({dateFormat: 'yy-mm-dd'});

			
});
