/*jshint unused: false*/
/*jshint -W117 */
'use strict';

/**
 * @ngdoc function
 * @name sociallinderApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the sociallinderApp
 */
angular.module('sociallinderApp')
  .controller('LoginCtrl', function ($scope, api, AuthenticationService, $location, $state, $cookies, $rootScope) {
    $(".date-picker").datepicker({dateFormat: 'yy-mm-dd'});
    //data
    $scope.suggestedProjects = {};
    //$scope.select = {};



    $scope.submit = function(email,password) {
        AuthenticationService.Login(email, password, function (response) {
            if (response.success) {
                console.log(response);
                
                AuthenticationService.SetCredentials(response.user); //should be username, password not email... 

                $location.path('/');
            } else {
                console.log(response.message);
            }
        });
    };

    $scope.isUserLoggedIn = function(){
        var credentials = AuthenticationService.getCredentials();
        if(credentials !== undefined){
            return true;
            //$rootScope.globals.currentUser.user = User Object
        }
        else{
            return false;
        }
    };

    if ($scope.isUserLoggedIn()){
      $scope.birthdate = new Date($rootScope.globals.currentUser.user.birthDate);
      $scope.birthdate = $scope.birthdate.getFullYear()+"-"+("0" + ($scope.birthdate.getMonth()+1)).slice(-2)+"-"+("0" + $scope.birthdate.getDate()).slice(-2); 

      if ($rootScope.globals.currentUser.user.organization)
        $scope.userType = "Organization";
      else
        $scope.userType = "Private Person";
      if ($rootScope.globals.currentUser.user.projectType !=undefined){
        for (var i=0; i<$rootScope.globals.currentUser.user.projectType.length; i++){
          if($rootScope.globals.currentUser.user.projectType[i]!=null && $rootScope.globals.currentUser.user.projectType[i]!="false" && $rootScope.globals.currentUser.user.projectType[i]!="null"){
            eval("$scope.projectType"+i+"="+'"'+$rootScope.globals.currentUser.user.projectType[i]+'"'); 
          }
          else
            {eval("$scope.projectType"+i+"=null");}
        }
      } 
    }

    $scope.logout = function(){
        AuthenticationService.clearCredentials();
    };

    $scope.updateUser = function() {
        // body...
        // save updates to user object..?
        // trigger getProjectsByType
        var temp = $rootScope.globals.currentUser.user;
        var prtype = [];
        prtype.push($scope.projectType0,$scope.projectType1, $scope.projectType2, $scope.projectType3, $scope.projectType4);        

        var saveUserObject= {
          "address": temp.address,
          "birthDate": $scope.birthdate,
          "email": temp.email,
          "organization": temp.organization,
          "userId": temp.userId,
          "userName": temp.userName,
          "password": temp.password,
          "projectType": temp.organization ? null : prtype,
          "organizationObject":{
            "id": temp.organization ? temp.organizationObject.id : null,
            "organizationTitle": temp.organization ? temp.organizationObject.organizationTitle : null,
            "address": temp.organization ? temp.organizationObject.address : null,
            "phoneNumber": temp.organization ? temp.organizationObject.phoneNumber : null
          }
        };

      api.saveUserDetails(saveUserObject).then(function(userObject){
        console.log(userObject);
        AuthenticationService.SetCredentials(userObject);
        alert("Geänderte Benutzerdaten gespeichert!"); //User details changed
        //$state.go($state.current, {}, {reload: true}); //Doesn't function as $rootscope is not refreshed. Implement full function in $cookies 
    }, function(){console.log("error");});
    };

    $scope.suggestProjects = function() {        
      //suggest Projects that satisfy filter criterion
      if ($rootScope.globals.currentUser.user.projectType[0]) { //check if type is not empty
          api.getProjectsByType({"Project Type" : [ $rootScope.globals.currentUser.user.projectType[0], $rootScope.globals.currentUser.user.projectType[1], $rootScope.globals.currentUser.user.projectType[2] ]}).then(function(projectInstances){
             var projects = [];
             for(var i=0; i < projectInstances.length; i++){
               var instance = projectInstances[i];
                projects.push({
                id: instance.id,
                title: instance.projectTitle,
                description: instance.projectDescription,
                sponsor: instance.organizationId,
                imageUrl: instance.imageBlobKey
              }); 
                //console.log('Pushing one element to scope with title ' + instance.projectTitle + '.');
             }
         $scope.suggestedProjects = projects;
        });
      }
      else{
        //Alternative: Get all?
        alert('Bitte gib ein Soziales Anliegen an!'); //'Please specify a social cause!'
      }
    };

      if($scope.projectType0!=null){
            $scope.EnvironmentIcon="images/images/Icons/icon_environment_pink.png";
            $scope.EnvironmentSet=true;
        }
        else{
            $scope.EnvironmentIcon="images/Icons/icon_environment_black.png";
            $scope.EnvironmentSet=false;
        }

       if($scope.projectType1!=null){
            $scope.AnimalsIcon="images/Icons/icon_animals_pink.png";
            $scope.AnimalsSet=true;
        }
        else{
            $scope.AnimalsIcon="images/Icons/icon_animals_black.png";
            $scope.AnimalsSet=false;
        }

        if($scope.projectType2!=null){
            $scope.ChildrenIcon="images/Icons/icon_children_pink.png";
            $scope.ChildrenSet=true;
        }
        else{
            $scope.ChildrenIcon="images/Icons/icon_children_black.png";
            $scope.ChildrenSet=false;
        }

        if($scope.projectType3!=null){
            $scope.RefugeesIcon="images/Icons/icon_refugees_pink.png";
            $scope.RefugeesSet=true;
        }
        else{
            $scope.RefugeesIcon="images/Icons/icon_refugees_black.png";
            $scope.RefugeesSet=false;
        }

        if($scope.projectType4!=null){
            $scope.VariousIcon="images/Icons/icon_resources_pink.png";
            $scope.VariousSet=true;
        }
        else{
            $scope.VariousIcon="images/Icons/icon_resources_black.png";
            $scope.VariousSet=false;
        }

    $scope.iconClick = function(type){
      if (type==1){
        if (!$scope.EnvironmentSet){
          $scope.EnvironmentIcon="images/Icons/icon_environment_pink.png";
          $scope.EnvironmentSet=true;
          $scope.projectType0="Umwelt";
        }
        else{
          $scope.EnvironmentIcon="images/Icons/icon_environment_black.png";
          $scope.EnvironmentSet=false;
          $scope.projectType0=null;
        } 
      }

      else if (type==2){
        if (!$scope.AnimalsSet){
          $scope.AnimalsIcon="images/Icons/icon_animals_pink.png";
          $scope.AnimalsSet=true;
          $scope.projectType1="Tierschutz";
        }
        else{
          $scope.AnimalsIcon="images/Icons/icon_animals_black.png";
          $scope.AnimalsSet=false;
          $scope.projectType1=null;
        } 
      }

      else if (type==3){
        if (!$scope.ChildrenSet){
          $scope.ChildrenIcon="images/Icons/icon_children_pink.png";
          $scope.ChildrenSet=true;
          $scope.projectType2="Kinder";
        }
        else{
          $scope.ChildrenIcon="images/Icons/icon_children_black.png";
          $scope.ChildrenSet=false;
          $scope.projectType2=null;
        } 
      }

      else if (type==4){
        if (!$scope.RefugeesSet){
          $scope.RefugeesIcon="images/Icons/icon_refugees_pink.png";
          $scope.RefugeesSet=true;
          $scope.projectType3="Flüchtlinge";
        }
        else{
          $scope.RefugeesIcon="images/Icons/icon_refugees_black.png";
          $scope.RefugeesSet=false;
          $scope.projectType3=null;
        } 
      }

      else if (type==5){
        if (!$scope.VariousSet){
          $scope.VariousIcon="images/Icons/icon_resources_pink.png";
          $scope.VariousSet=true;
          $scope.projectType4="Verschiedenes";
        }
        else{
          $scope.VariousIcon="images/Icons/icon_resources_black.png";
          $scope.VariousSet=false;
          $scope.projectType4=null;
        } 
      }


    };

  }); 