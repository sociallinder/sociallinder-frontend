'use strict';

/**
 * @ngdoc function
 * @name sociallinderApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the sociallinderApp
 */
angular.module('sociallinderApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
