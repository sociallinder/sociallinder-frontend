/*jshint unused: false*/
/*jshint -W117 */

'use strict';

/**
 * @ngdoc function
 * @name sociallinderApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sociallinderApp
 */
angular.module('sociallinderApp')
  .controller('NavbarCtrl', function ($scope, $rootScope, api, AuthenticationService, $cookies) {

    //this should not be here... 
    (function initController() {
            // reset login status
            // AuthenticationService.ClearCredentials();
        var userId = $cookies.get('pinkelephantLogin');
        console.log(userId);
        if(userId !== undefined){
          AuthenticationService.LoginWithId(userId, function(response){
            if (response.success) {
                console.log(response);                
                AuthenticationService.SetCredentials(response.user); //should be username, password not email... 
            } else {
                console.log(response.message);
            }
          });
        }
    })();

  	//constants
  	this.HOME = 'home';
  	this.PROJECTS = 'projects';
  	this.IDEAS = 'ideas';
  	this.SUCCESS_STORIES = 'success_stories';
  	this.HOW_IT_WORKS = 'how_it_works';
    this.COMMUNITY = 'community';
  	this.ABOUT = 'about';
  	this.USERPROFILE = 'userprofile';

    this.loginTitle = function(){
      return(AuthenticationService.getLoginLabel());
    };
  	//variables
  	this.activeNavitem = this.home;

  	//functions
 	this.isActive = function(item){
 		return (item === this.activeNavitem);
 	};
 	this.setIsActive = function(item){
 		this.activeNavitem = item;
 	};
    
  });
