/*jshint unused: false*/
/*jshint -W117 */

'use strict';

/**
 * @ngdoc function
 * @name sociallinderApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sociallinderApp
 */
angular.module('sociallinderApp')
  .controller('HomeCtrl', function ($scope, $rootScope, $http, api, $location, $state) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

//     console.log($http.defaults.headers.common['Authorization']);
    //On initial load of Controller
    //Load all projects
    api.getAllProjects().then(function(projectInstances){
      var projects = [];

      var funcGetNeedsMet = function(projectId, index){
        api.getPercentageNeedMetByProjectID({"ProjectID": projectId})
        .then(function(response){
            projects[index].needsMet = response.message/100;
        });
      };

      for(var i=0; i < projectInstances.length; i++){
        var instance = projectInstances[i];
        projects.push({
          id: instance.id,
          title: instance.projectTitle,
          description: instance.projectDescription,
          sponsor: instance.organizationId ,
          imageUrl: instance.imageUrl,
          ideaFrom: instance.user.userName,
          needsMet: 0 //will be calculated separately
        }); 
        funcGetNeedsMet(instance.id, i);
      }
      $scope.projects = projects;
    });

    //sample data
    /*this.projects2 = [
      {
        id: '123',
        title: 'Some cool new Project',
        description: 'some description for the project. there can be quite some stuff in here. We could think of any kind of description there is on this planet. But it should not exceed 4 lines I guess.',
        sponsor: 'The Unicorns!'
      },
      {
        id: '124',
        title: 'Some second cool Project',
        description: 'some description for the project. there can be quite some stuff in here. We could think of any kind of description there is on this planet. But it should not exceed 4 lines I guess.',
        sponsor: 'The Unicorns!'
      },
      {
        id: '125',
        title: 'Last but yet cool new Project',
        description: 'some description for the project. there can be quite some stuff in here. We could think of any kind of description there is on this planet. But it should not exceed 4 lines I guess.',
        sponsor: 'The Unicorns!'
      }
    ];*/

    this.ideas = [
      {
        id: '126',
        title: 'Eine coole neue Idee',
        description: 'Eine Beschreibung der Idee. Hier können sehr viele Infos über die Idee stehen und wie diese zu einem Projekt gemacht werden kann, wer dazu beitragen könnte, was man an Beiträgen bräuchte, wem damit geholfen wird, etc.',
        ideaFrom: 'Die Einhörner!'
      },
      {
        id: '127',
        title: 'Noch eine coole Idee',
        description: 'Eine Beschreibung der Idee. Hier können sehr viele Infos über die Idee stehen und wie diese zu einem Projekt gemacht werden kann, wer dazu beitragen könnte, was man an Beiträgen bräuchte, wem damit geholfen wird, etc.',
        ideaFrom: 'Die Einhörner!'
      },
      {
        id: '128',
        title: 'Und eine dritte coole Idee',
        description: 'Eine Beschreibung der Idee. Hier können sehr viele Infos über die Idee stehen und wie diese zu einem Projekt gemacht werden kann, wer dazu beitragen könnte, was man an Beiträgen bräuchte, wem damit geholfen wird, etc.',
        ideaFrom: 'Die Einhörner!'
      }
    ];

    this.success_stories = [
      {
        id: '129',
        title: 'Eine geniale Erfolgsgeschichte',
        description: 'Eine Beschreibung der Erfolgsgeschichte. Hier können sehr viele Infos über die Idee stehen und wie diese zu einem Projekt gemacht werden kann, wer dazu beitragen könnte, was man an Beiträgen bräuchte, wem damit geholfen wird, etc.',
        ideaFrom: 'Die Einhörner!'
      },
      {
        id: '130',
        title: 'Noch eine geniale Erfolgsgeschichte',
        description: 'Eine Beschreibung der Erfolgsgeschichte. Hier können sehr viele Infos über die Idee stehen und wie diese zu einem Projekt gemacht werden kann, wer dazu beitragen könnte, was man an Beiträgen bräuchte, wem damit geholfen wird, etc.',
        ideaFrom: 'Die Einhörner!'
      },
      {
        id: '131',
        title: 'Eine weitere tolle Erfolgsgeschichte',
        description: 'Eine Beschreibung der Erfolgsgeschichte. Hier können sehr viele Infos über die Idee stehen und wie diese zu einem Projekt gemacht werden kann, wer dazu beitragen könnte, was man an Beiträgen bräuchte, wem damit geholfen wird, etc.',
        ideaFrom: 'Die Einhörner!'
      }
    ];

    $scope.go = function (state, params) {
      console.log(state+"/"+params);
      $state.go(state,params);
      // $location.path(path);
    };
  });
