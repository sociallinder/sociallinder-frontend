'use strict';

/**
 * @ngdoc function
 * @name sociallinderApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the sociallinderApp
 */
angular.module('sociallinderApp')
  .controller('CommunityCtrl', function ($scope) {
    
    $scope.boxes=[{
    	name:"Max Mustermann",
    	title:"Flüchtlingshilfeprojekt in Mannheim",
    	description:"Ich habe durch die Mitarbeit an diesem Projekt sehr viele Menschen mit vielen verschiedenen Hintergründen kennen gelernt, die alle der gleichen Überzeugung sind: Flüchtlinge sind Menschen, die in den vergangenen Monaten viel Schlimmes erlebt haben und nun darauf angewiesen sind, dass sie Unterstützung von anderen Menschen bekommen um ihre Sicherheit und Gesundheit zu ermöglichen. Indem ich ein Unterstützer dieses Projekts geworden bin, habe ich viele dieser Menschen kennen gelernt, ihre Geschichten gehört und war oft sehr berührt von ihren Schicksalen.",
    	like:"15"
    	}, {
    	name:"Erika Mustermann",
    	title:"Kinderferienfreizeit für Waisenkinder",
    	description:"Durch meine Unterstützung bei der Kinderferienfreizeit MannheimLacht habe ich einige wunderschöne Tage mit vielen intelligenten und aufgeschlossenen Kindern verbringen können. Ich habe neu gelernt, wie wertvoll es ist ein Kind zum Lachen zu bringen und wie viel Freude es einem selbst schenkt. Ich werde beim nächsten Mal sofort wieder mitmachen, mir hat es mindestens so viel Spaß gemacht wie den jungen Teilnehmern!",
    	like:"7"
    	}];
    
  });