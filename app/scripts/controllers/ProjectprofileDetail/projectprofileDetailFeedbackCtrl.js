/*jshint unused: false*/
/*jshint -W117 */

'use strict';

/**
 * @ngdoc function
 * @name sociallinderApp.controller:MainCtrl
 * @description
 * # FeedbackCtrl
 * Controller of the sociallinderApp
 */
angular.module('sociallinderApp')
  .controller('ProjectprofileDetailFeedbackCtrl', function ($scope, $rootScope, AuthenticationService, api, $location, $state) {
  	
  	$scope.sendFeedback = function() {
  		$scope.projectFeedback = '';
  		alert("Vielen Dank! Dein Feedback wurde gespeichert.");
    };

        $scope.isUserLoggedIn = function(){
        var credentials = AuthenticationService.getCredentials();
        if(credentials !== undefined){
            return true;
            //$rootScope.globals.currentUser.user = User Object
        }
        else{
            return false;
        }
    };
  });