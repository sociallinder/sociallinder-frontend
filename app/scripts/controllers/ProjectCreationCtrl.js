/*jshint unused: false*/
/*jshint -W117 */
'use strict';


/**
 * ProjectDTO interface:
 * Only fill the stuff you a) know about or b) want to change
 * the only mandatory field if you want to change something is the id!
 */

/**
 * 
 {
   "id": "12345",
   "projectTitle": "One Sample Title",
   "projectDescription": "Some Sample Description",
   "successFactors": [
    "One Successfactor",
    "Second Successfactor"
   ],
   "location": "Some Location",
   "projectType": "Some Projecttype",
   "organizationId": "987",
   "userId": "234",
   "projectDate": "2015-10-25T01:45:18.162+02:00",
   "imageUrl": "http://this.is.some.image.url",
   "keyNeeds": [
    {
     "id": "123",
     "title": "Some Keyneed",
     "description": "Some keyneed description",
     "totalNeeds": 10,
     "needsMet": 2,
     "supporters": [
      "234",
      "456"
     ]
    },
    {
     "id": "123",
     "title": "Some Keyneed",
     "description": "Some keyneed description",
     "totalNeeds": 10,
     "needsMet": 2,
     "supporters": [
      "234",
      "456"
     ]
    }
   ],
   "events": [
    {
     "id": "456",
     "title": "Some Event",
     "description": "Some Event description",
     "comments": [
      {
       "id": "123",
       "message": "Some Comment",
       "userId": "456",
       "createdDate": "2015-10-25T01:45:18.162+02:00"
      },
      {
       "id": "123",
       "message": "Some Comment",
       "userId": "456",
       "createdDate": "2015-10-25T01:45:18.162+02:00"
      }
     ],
     "likes": [
      {
       "id": "123",
       "userId": "566",
       "createdDate": "2015-10-25T01:45:18.162+02:00"
      },
      {
       "id": "123",
       "userId": "566",
       "createdDate": "2015-10-25T01:45:18.162+02:00"
      }
     ],
     "keyNeeds": [
      {
       "id": "123",
       "title": "Some Keyneed",
       "description": "Some keyneed description",
       "totalNeeds": 10,
       "needsMet": 2,
       "supporters": [
        "234",
        "456"
       ]
      },
      {
       "id": "123",
       "title": "Some Keyneed",
       "description": "Some keyneed description",
       "totalNeeds": 10,
       "needsMet": 2,
       "supporters": [
        "234",
        "456"
       ]
      }
     ]
    },
    {
     "id": "456",
     "title": "Some Event",
     "description": "Some Event description",
     "comments": [
      {
       "id": "123",
       "message": "Some Comment",
       "userId": "456",
       "createdDate": "2015-10-25T01:45:18.162+02:00"
      },
      {
       "id": "123",
       "message": "Some Comment",
       "userId": "456",
       "createdDate": "2015-10-25T01:45:18.162+02:00"
      }
     ],
     "likes": [
      {
       "id": "123",
       "userId": "566",
       "createdDate": "2015-10-25T01:45:18.162+02:00"
      },
      {
       "id": "123",
       "userId": "566",
       "createdDate": "2015-10-25T01:45:18.162+02:00"
      }
     ],
     "keyNeeds": [
      {
       "id": "123",
       "title": "Some Keyneed",
       "description": "Some keyneed description",
       "totalNeeds": 10,
       "needsMet": 2,
       "supporters": [
        "234",
        "456"
       ]
      },
      {
       "id": "123",
       "title": "Some Keyneed",
       "description": "Some keyneed description",
       "totalNeeds": 10,
       "needsMet": 2,
       "supporters": [
        "234",
        "456"
       ]
      }
     ]
    }
   ]
  }
 */

/**
 * @ngdoc function
 * @name sociallinderApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the sociallinderApp
 */
angular.module('sociallinderApp')
  .controller('ProjectCreationCtrl', function ($scope, api, $location, $state, AuthenticationService, fileUpload) {

    $(".date-picker").datepicker({dateFormat: 'yy-mm-dd'});

    $scope.project = {
      projectTitle: "",
      projectMission: "",
      projectDescription: "",
      successFactors: [{
        text: ""
      }],
      location: "",
      projectType: "",
      organizationId: "",
      userId: "", //use moment.js to format dates: format needed is 2015-10-25T01:45:18.162+02:00 projectDate: "",
      keyNeeds: [
        {
         title: "",
         description: "",
         dimension: "",
         totalNeeds: 0,
         needsMet: 0
        }
      ]
    };

    // //Project Object skeleton for API call
    // $scope.projectObject = {
    //   //"id":"12345", //not necessary, will be generated
    //   "location":"",
    //   "organizationId":"4738294",
    //   "projectDescription":"",
    //   "projectTitle":"",
    //   "projectType":"",
    //   "totalAmount": 2500, //not necessary, will be calculated
    //   "totalFunded":  389, //not necessary, will be calculated
    //   "userId":"3298489",
    //   "organizationDTO": {
    //     "address":"Mannheimer Musterstr. 1",
    //     "email":"musteremail@muster.de",
    //     "id":"4738294",
    //     "organizationDescription":"This is a very cool oranization.",
    //     "organizationTitle":"ExampleOrga",
    //     "ownerOfOrganization": {
    //       "address":"Musterweg 1",
    //       "email":"musteremail2@muster.de",
    //       "mainGoal":"Support social causes",
    //       "password":"empty_pw",
    //       "personalwebsite":"",
    //       "totalFundedAmount": 190,
    //       "userId":"2092389",
    //       "userName":"GreatPerson" 
    //     },
    //     "phoneNumber": 1111111111 
    //   },
    //   "userDTO": {
    //     "address":"Musterweg 1",
    //       "email":"musteremail2@muster.de",
    //       "mainGoal":"Support social causes",
    //       "password":"empty_pw",
    //       "personalwebsite":"",
    //       "totalFundedAmount": 190,
    //       "userId":"2092389",
    //       "userName":"GreatPerson"
    //   }
    // };
    
    //controller variables
    $scope.page = 1;
    
   	//Controller functions
    
    $scope.addSuccessFactor = function(){
      $scope.project.successFactors.push({ message: ($scope.project.successFactors.length+1)+". Successfactor"});
    };
    $scope.deleteSuccessfactor = function(index){
      if($scope.project.successFactors.length > 1){
        $scope.project.successFactors.splice(index,1);
      }
    };

    $scope.addKeyNeed = function(){
      var keyneed = {
         title: "",
         description: "",
         totalNeeds: 0,
         needsMet: 0
      };
      keyneed.title = ($scope.project.keyNeeds.length+1)+". Key Need";
      $scope.project.keyNeeds.push(keyneed);
    };

    $scope.saveProject = function(){
      //prepare project for saving
      $scope.project.userId = AuthenticationService.getCredentials().user.userId;

      $scope.project.startDate = new Date($scope.project.startDate);
      $scope.project.endDate = new Date($scope.project.endDate);

      console.log($scope.project);
      // console.log($scope.project);
      //console.log($scope.projectObject);
      api.saveProject($scope.project).then(
        function(response){
        
          api.getImageUploadUrl(response.id)
            .then(function(uploadUrl){
              console.log("ImageUploadUrl: "+uploadUrl);
              var file = $scope.picture;
              console.log("File to be uploaded:");
              console.log(file);

              fileUpload.uploadFileToUrl(file, uploadUrl)
                .then(function(imageUrl){
                  console.log("UploadFileToUrl returned: "+imageUrl);
                  var paramsText = { "projectId": response.id };
                  $state.go('projectprofile.campaign', paramsText);
                });
            });


        },
        function(reason){} //What to do when it is not successful?
      );
    };

    $scope.isUserLoggedIn = function(){
      var credentials = AuthenticationService.getCredentials();
      if(credentials !== undefined){
        return true;
      }
      else{
        return false;
      }
    };

    $scope.isUserOrganizationRepr = function(){
      var credentials = AuthenticationService.getCredentials();
      if(credentials !== undefined){
        return credentials.user.organization;
      }
      return false;
      
    };











    $scope.question = {
    options: [
      { date: '', description: '' }
    ],
    option_new: { date: '', description: '' }
    };

    $scope.add = function() {
    // add the new option to the model
    $scope.question.options.push($scope.question.option_new);
    // clear the option.
    $scope.question.option_new = { date: '', description: '' };
    };

    $scope.check = function(value) {
    if (value>0 && value<3){ 
       return true;
     }
    else{
        return false;
      }  
    };

    $scope.go = function (state, params) {
      console.log(state+"/"+params);
      $state.go(state,params);
      // $location.path(path);
    };

    // $scope.saveProject = function(){
    //   //console.log($scope.projectObject);
    //   api.saveProject($scope.projectObject).then(
    //     function(response){
    //       alert('Project was successfully saved!');
    //       //console.log(response.id);
    //       var paramsText = { "projectId": response.id };
    //       $state.go('projectprofile.campaign', paramsText);
    //     },
    //     function(reason){} //What to do when it is not successful?
    //   );
    // };
    
  });
