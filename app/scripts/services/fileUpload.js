'use strict';

angular.module('sociallinderApp')
	.service('fileUpload', ['$http', '$q', function ($http, $q) {
    this.uploadFileToUrl = function(file, uploadUrl){
        var defer = $q.defer();

        var fd = new FormData();
        fd.append('myFile', file);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .then(function(resp){
            console.log("the response from calling "+uploadUrl+" is:");
            console.log(resp);
        	defer.resolve(resp.data.imageUrl);
        	// console.log(resp);
        }, function(reason){
        	console.log(reason);
        });

        return defer.promise;
    };
}]);