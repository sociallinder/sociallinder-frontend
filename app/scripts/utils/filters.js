/*jshint unused: false*/
/*jshint -W117 */

'use strict';


angular.module('sociallinderApp')
	.filter('percentage', ['$filter', function ($filter) {
	  return function (input, decimals) {
	    return $filter('number')(input * 100, decimals) + '%';
	  };
	}]);