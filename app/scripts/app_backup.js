'use strict';

/**
 * @ngdoc overview
 * @name sociallinderApp
 * @description
 * # sociallinderApp
 *
 * Main module of the application.
 */
angular
  .module('sociallinderApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.router',
  ])
  .config(function ($routeProvider) {
    $routeProvider
    //-----------main pages -------------------------------------
      .when('/home', {
        templateUrl: 'views/partials/home.html',
        controller: 'HomeCtrl',
        controllerAs: 'home'
      })
      .when('/about', {
        templateUrl: 'views/partials/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .when('/projects', {
        templateUrl: 'views/partials/projects.html',
        controller: 'ProjectsCtrl',
        controllerAs: 'projects'
      })
      .when('/ideas', {
        templateUrl: 'views/partials/ideas.html',
        controller: 'IdeasCtrl',
        controllerAs: 'ideas'
      })
      .when('/success_stories', {
        templateUrl: 'views/partials/success_stories.html',
        controller: 'SuccessStoriesCtrl',
        controllerAs: 'success_stories'
      })
      .when('/how_it_works', {
        templateUrl: 'views/partials/how_it_works.html',
        controller: 'HowItWorksCtrl',
        controllerAs: 'how_it_works'
      })
      .when('/userprofile', {
        templateUrl: 'views/partials/userprofile.html',
        controller: 'UserprofileCtrl',
        controllerAs: 'userprofile'
      })
      //-----------project / item / success story (single entity) pages -------------------------------------
      .when('/projectprofile/:projectId', {
        templateUrl: 'views/partials/projectprofile.html',
        controller: 'ProjectprofileCtrl',
        controllerAs: 'projectprofile'
      })
      //-----------other pages -------------------------------------
      .otherwise({
        redirectTo: '/home'
      });

  });
