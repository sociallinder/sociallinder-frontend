'use strict';

/**
 * @ngdoc overview
 * @name sociallinderApp
 * @description
 * # sociallinderApp
 *
 * Main module of the application.
 */
angular
  .module('sociallinderApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.router',
    'ui.bootstrap',
    'angular-nicescroll'
  ])
  .config(function ($stateProvider, $urlRouterProvider) {
    // For any unmatched url, send to /route1
      $urlRouterProvider.otherwise("/home");

      $stateProvider
        .state('home', {
          url: '/home',
          templateUrl: 'views/partials/home.html',
          controller: 'HomeCtrl',
          controllerAs: 'home'
        })
        .state('about', {
          url: '/about',
          templateUrl: 'views/partials/about.html',
          controller: 'AboutCtrl',
          controllerAs: 'about'
        })
        .state('projects', {
          url: '/projects',
          templateUrl: 'views/partials/projects.html',
          controller: 'ProjectsCtrl',
          controllerAs: 'projects'
        })
        .state('ideas', {
          url: '/ideas',
          templateUrl: 'views/partials/ideas.html',
          controller: 'IdeasCtrl',
          controllerAs: 'ideas'
        })
        .state('success_stories', {
          url: '/success_stories',
          templateUrl: 'views/partials/success_stories.html',
          controller: 'SuccessStoriesCtrl',
          controllerAs: 'success_stories'
        })
        .state('how_it_works', {
          url: '/how_it_works',
          templateUrl: 'views/partials/how_it_works.html',
          controller: 'HowItWorksCtrl',
          controllerAs: 'how_it_works'
        })
        .state('userprofile', {
          url: '/userprofile',
          templateUrl: 'views/partials/userprofile.html',
          controller: 'UserprofileCtrl',
          controllerAs: 'userprofile'
        })
        .state('community', {
          url: '/community',
          templateUrl: 'views/partials/community.html',
          controller: 'CommunityCtrl',
          controllerAs: 'community'
        })
        .state('login', {
          url: '/login',
          templateUrl: 'views/partials/loginRegister/userprofile.html',
          controller: 'LoginCtrl',
          controllerAs: 'login'
        })
        .state('register', {
          url: '/register',
          templateUrl: 'views/partials/loginRegister/register.html',
          controller: 'RegisterCtrl',
          controllerAs: 'register'
        })
        .state('projectcreation', {
          url: '/projectcreation',
          templateUrl: 'views/partials/project_creation.html',
          controller: 'ProjectCreationCtrl',
          controllerAs: 'projectcreation'
        })


      //-----------project / item / success story (single entity) pages -------------------------------------
      //
        .state('projectprofile', {
          url: '/project/:projectId',
          templateUrl: 'views/partials/projectprofile.html',
          controller: 'ProjectprofileCtrl',
          controllerAs: 'projectprofile'
        })
              .state('projectprofile.campaign', {
                url:'/campaign',
                templateUrl:'views/partials/projectprofile_detailpages/campaign.html',
                controller: 'ProjectprofileDetailCampaignCtrl',
                controllerAs: 'campaign'
              })
              .state('projectprofile.events', {
                url:'/events',
                templateUrl:'views/partials/projectprofile_detailpages/events.html',
                controller: 'ProjectprofileDetailEventsCtrl',
                controllerAs: 'events'
              })
              .state('projectprofile.feedback', {
                url:'/feedback',
                templateUrl:'views/partials/projectprofile_detailpages/feedback.html',
                controller: 'ProjectprofileDetailFeedbackCtrl',
                controllerAs: 'feedback'
              })
              .state('projectprofile.contact', {
                url:'/contact',
                templateUrl:'views/partials/projectprofile_detailpages/contact.html',
                controller: 'ProjectprofileDetailContactCtrl',
                controllerAs: 'contact'
              })
        ;

  });