'use strict';

angular.module('sociallinderApp')
	.directive('navbarDirective', function(){

		return {
			restrict: 'E',
			templateUrl: 'views/templates/navigation/template_navbar.html'
		};

	});