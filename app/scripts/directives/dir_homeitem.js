'use strict';

angular.module('sociallinderApp')
	.directive('homeitemDirective', function(){

		return {
			restrict: 'E',
			scope:{
				iteminfo: '=iteminfo' 
			},
			templateUrl: 'views/templates/homeItems/home_item.html'
		};

	});