
'use strict';

/**
 * @ngdoc function
 * @name sociallinderApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sociallinderApp
 */
angular.module('sociallinderApp')
	.directive('errSrc', function() {
  return {
    link: function(scope, element, attrs) {
    	
      element.bind('error', function() {
        if (attrs.src !== attrs.errSrc) {
          attrs.$set('src', attrs.errSrc);
        }
      });

      attrs.$observe('ngSrc', function(value) {
        if (!value && attrs.errSrc) {
          attrs.$set('src', attrs.errSrc);
        }
      });

    }
  };
});